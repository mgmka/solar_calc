# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from main.models import *

admin.site.register(maxjan)
admin.site.register(maxfeb)
admin.site.register(maxmar)
admin.site.register(maxapr)
admin.site.register(maxmay)
admin.site.register(maxjun)
admin.site.register(maxjul)
admin.site.register(maxaug)
admin.site.register(maxsep)
admin.site.register(maxoct)
admin.site.register(maxnov)
admin.site.register(maxdec)

admin.site.register(solarjan)
admin.site.register(solarfeb)
admin.site.register(solarmar)
admin.site.register(solarapr)
admin.site.register(solarmay)
admin.site.register(solarjun)
admin.site.register(solarjul)
admin.site.register(solaraug)
admin.site.register(solarsep)
admin.site.register(solaroct)
admin.site.register(solarnov)
admin.site.register(solardec)
