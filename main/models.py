# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class maxjan(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class maxfeb(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class maxmar(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class maxapr(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class maxmay(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class maxjun(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class maxjul(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class maxaug(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class maxsep(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class maxoct(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class maxnov(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class maxdec(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class solarjan(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class solarfeb(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class solarmar(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class solarapr(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class solarmay(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class solarjun(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class solarjul(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class solaraug(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class solarsep(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class solaroct(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class solarnov(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()


class solardec(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    value = models.FloatField()