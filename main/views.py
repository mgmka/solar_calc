# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from models import *
from django.http import JsonResponse

# Create your views here.

geo_params = {
    'solar': {
        'cols': 839,
        'rows': 680,
        'ln': 112.02500534058,
        'lt': -44.021758556366,
        'step': 0.049995422363281
    },
    'temp': {
        'cols': 1681,
        'rows': 1361,
        'ln': 112,
        'lt': -44,
        'step': 0.025
    },
}
solar_max_ln = geo_params['solar']['cols'] * geo_params['solar']['step']
solar_max_lt = geo_params['solar']['rows'] * geo_params['solar']['step']


def index(request):
    return render(request, 'index.html')

#def find_good_value(fp, lt, ln, max_lt, max_ln):


def get_value(fname, steps_ln, steps_lt, type):
    print steps_ln, steps_lt
    result = 0
    with open("./data/" + fname) as fp:
        for i, line in enumerate(fp):
            if i == steps_lt:
                values = line.split(' ')
                result = values[steps_ln]
                if result == '-9999':
                    if type:
                        if steps_lt > geo_params['temp']['rows']/2:
                            steps_lt -= 1
                        else:
                            steps_lt += 1
                        if steps_ln > geo_params['temp']['cols']/2:
                            steps_ln -= 1
                        else:
                            steps_ln += 1
                    else:
                        if steps_lt > geo_params['solar']['rows']/2:
                            steps_lt -= 1
                        else:
                            steps_lt += 1
                        if steps_ln > geo_params['solar']['cols']/2:
                            steps_ln -= 1
                        else:
                            steps_ln += 1
                    result = get_value(fname,steps_ln, steps_lt, type)
                else:
                    return result

    return result


def api(request):
    data = {}
    solar = []
    temperature = []
    if request.method == 'GET':
        latitude = request.GET.get('lt', 0)
        longitude = request.GET.get('ln', 0)
        if latitude and longitude:
            solar_steps_ln = int((float(longitude) - float(geo_params['solar']['ln'])) // geo_params['solar']['step'])
            solar_steps_lt = geo_params['solar']['rows'] - int(
                (float(latitude) - float(geo_params['solar']['lt'])) // geo_params['solar']['step']) + 6
            temp_steps_ln = int((float(longitude) - float(geo_params['temp']['ln'])) // geo_params['temp']['step'])
            temp_steps_lt = geo_params['temp']['rows'] - int(
                (float(latitude) - float(geo_params['temp']['lt'])) // geo_params['temp']['step']) + 6

            solar.append(get_value('solarjan.txt', solar_steps_ln, solar_steps_lt, 0))
            solar.append(get_value('solarfeb.txt', solar_steps_ln, solar_steps_lt, 0))
            solar.append(get_value('solarmar.txt', solar_steps_ln, solar_steps_lt, 0))
            solar.append(get_value('solarapr.txt', solar_steps_ln, solar_steps_lt, 0))
            solar.append(get_value('solarmay.txt', solar_steps_ln, solar_steps_lt, 0))
            solar.append(get_value('solarjun.txt', solar_steps_ln, solar_steps_lt, 0))
            solar.append(get_value('solarjul.txt', solar_steps_ln, solar_steps_lt, 0))
            solar.append(get_value('solaraug.txt', solar_steps_ln, solar_steps_lt, 0))
            solar.append(get_value('solarsep.txt', solar_steps_ln, solar_steps_lt, 0))
            solar.append(get_value('solaroct.txt', solar_steps_ln, solar_steps_lt, 0))
            solar.append(get_value('solarnov.txt', solar_steps_ln, solar_steps_lt, 0))
            solar.append(get_value('solardec.txt', solar_steps_ln, solar_steps_lt, 0))

            #todo refactor list adds
            temperature.append(get_value('maxjan.txt', temp_steps_ln, temp_steps_lt, 1))
            temperature.append(get_value('maxfeb.txt', temp_steps_ln, temp_steps_lt, 1))
            temperature.append(get_value('maxmar.txt', temp_steps_ln, temp_steps_lt, 1))
            temperature.append(get_value('maxapr.txt', temp_steps_ln, temp_steps_lt, 1))
            temperature.append(get_value('maxmay.txt', temp_steps_ln, temp_steps_lt, 1))
            temperature.append(get_value('maxjun.txt', temp_steps_ln, temp_steps_lt, 1))
            temperature.append(get_value('maxjul.txt', temp_steps_ln, temp_steps_lt, 1))
            temperature.append(get_value('maxaug.txt', temp_steps_ln, temp_steps_lt, 1))
            temperature.append(get_value('maxsep.txt', temp_steps_ln, temp_steps_lt, 1))
            temperature.append(get_value('maxoct.txt', temp_steps_ln, temp_steps_lt, 1))
            temperature.append(get_value('maxnov.txt', temp_steps_ln, temp_steps_lt, 1))
            temperature.append(get_value('maxdec.txt', temp_steps_ln, temp_steps_lt, 1))

            data['solar'] = solar
            data['temperature'] = temperature

            return JsonResponse(data)
    elif request.method == 'POST':
        pass
    return 0
