import json


def file_put_contents(data, filename, opt='a'):
    f = open(filename, opt)
    f.write(data)
    f.close()
    return 0


file_names = ['maxaug.txt', 'maxdec.txt', 'maxfeb.txt', 'maxjan.txt', 'maxjul.txt', 'maxjun.txt', 'maxmar.txt',
              'maxmay.txt', 'maxnov.txt', 'maxoct.txt', 'maxsep.txt', 'solaran.txt', 'solarapr.txt']

file_names1 = ['maxapr.txt', 'solaraug.txt', 'solardec.txt', 'solarfeb.txt', 'solarjan.txt', 'solarjun.txt',
               'solarjul.txt', 'solarmar.txt', 'solarmay.txt', 'solarnov.txt', 'solaroct.txt', 'solarsep.txt']

# file_name = 'maxaug.txt'

cols_count = ''
rows_count = ''
start_x = ''
start_y = ''
step = ''
no_data_value = ''
models = []
model = {}
fields = {}
i = 0
values = []
for file_name in file_names1:
    models = []
    print file_name
    with open('./' + file_name) as file:
        i = 0
        for index, line in enumerate(file):
            if index == 0:
                cols_count = line.replace('\n', '').split('         ')[1]
                print cols_count
            elif index == 1:
                rows_count = line.replace('\n', '').split('         ')[1]
                print rows_count
            elif index == 2:
                start_x = line.replace('\n', '').split('     ')[1]
                print start_x
            elif index == 3:
                start_y = line.replace('\n', '').split('     ')[1]
                print start_y
            elif index == 4:
                step = line.replace('\n', '').split('      ')[1]
                print step
            elif index == 5:
                no_data_value = line.replace('\n', '').split('  ')[1]
                print no_data_value
            else:
                values = line.replace(' \n', '').split(' ')
                for index_x, value in enumerate(values):
                    if value != '-9999':
                        i += 1
                        model = {}
                        fields = {}
                        model['model'] = 'main.' + file_name.split('.')[0]
                        model['pk'] = i
                        fields['longitude'] = float(start_x) + float(step) * index_x
                        fields['latitude'] = float(start_y) + (int(rows_count) - 1) * float(step) - float(step) * (
                                    index - 6)
                        # print value
                        try:
                            fields['value'] = float(value)
                        except:
                            print 'index - ' + str(index_x) + ' Exception - '
                            print values
                        # fields['data_type'] = file_name.split('.')[0]
                        model['fields'] = fields
                        models.append(model)

    file_put_contents(json.dumps(models), './test/'+file_name.split('.')[0] + '.json', 'w')

    # ncols
    # 839
    # nrows
    # 680
    # xllcorner
    # 112.02500534058
    # yllcorner - 44.021758556366
    # cellsize
    # 0.049995422363281
    # NODATA_value - 9999
