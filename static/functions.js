Vue.use(VueCharts);

Vue.filter('toCurrency', function (value) {
    if (typeof value !== "number") {
        return value;
    }
    var formatter = new Intl.NumberFormat('en-AU', {
        style: 'currency',
        currency: 'AUD',
        minimumFractionDigits: 0
    });
    return formatter.format(value);
});

Vue.component('date_picker', {
    props: ['value'],
    template: '#date_picker-template',
    mounted: function () {
        var vm = this;
        $(this.$el).pickadate({
            default: 'now',
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15, // Creates a dropdown of 15 years to control year,
            today: 'Today',
            clear: 'Clear',
            close: 'Ok',
            closeOnSelect: true // Close upon selecting a date,
        });
        $(this.$el).val(this.value)
            .trigger('change')
            // emit event on change.
            .on('change', function () {
                vm.$emit('input', vm.value);
            })
    },
    watch: {
        value: function (value) {
            // update value
            $(this.$el).val(value);
        }
    }
});


Vue.component('time_picker', {
    props: ['value'],
    template: '#time_picker-template',
    mounted: function () {
        var vm = this;
        $(this.$el).pickatime({
            default: 'now',
            fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
            twelvehour: false, // Use AM/PM or 24-hour format
            donetext: 'OK', // text for done-button
            cleartext: 'Clear', // text for clear-button
            canceltext: 'Cancel', // Text for cancel-button
            autoclose: false, // automatic close timepicker
            ampmclickable: true, // make AM PM clickable
            aftershow: function () {
            } //Function for after opening timepicker
        });
        $(this.$el).val(this.value)
            .trigger('change')
            // emit event on change.
            .on('change', function () {
                vm.$emit('input', vm.value);
            })
    },
    watch: {
        value: function (value) {
            // update value
            $(this.$el).val(value);
        }
    }
});


Vue.component('select2', {
    template: '#select2-template',
    mounted: function () {
        var vm = this;
        $(this.$el).material_select();
        $(this.$el)
            .val(this.value)
            .trigger('change')
            // emit event on change.
            .on('change', function () {
                vm.$emit('input', vm.value);
            })
    },
    watch: {
        value: function (value) {
            // update value
            $(this.$el).val(value);
        },
        options: function (options) {
            // update options
            $(this.$el).empty().select2({data: options})
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
});

app = new Vue({
    el: '#app',
    delimiters: ['[[', ']]'],
    data: {
        cons21: 0.4,
        show_data: false,
        center: '',
        latitude: '0',
        longitude: '0',
        address: '',
        supply_charge: '1.00',
        peak: '0.3',
        peak_start_time: '9:00',
        peak_end_time: '20:00',
        off_peak: '0.14',
        off_peak_start_time: '23:00',
        off_peak_end_time: '7:00',
        shoulder: '0.24',
        feed_in: '0.113',
        energy_use: '35',
        energy_use_off_peak: '5',
        energy_use_shoulder: '15',
        electricity_cost_increase: '5',
        energy_use_all_max: '100',
        //peak_no_sun: '',
        table_data: [{month: '', solar: '', temp: '', power: ''}],
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        data1: [65, 59, 80, 81, 56, 55, 40, 85, 93, 24, 45, 72],
        system_area: '0',
        panel_efficiency: false,
        with_battery: true,
        system_losses: '11.93',
        month: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        period_form:
            [{
                type: false,
                start_date: "1 January, 2017",
                end_date: "31 December, 2017",
                amount: '1500',
                tariff_type: 1
            }]
    },
    watch: {
        panel_efficiency: function () {
            this.recalculate();
        },
        system_area: function () {
            this.recalculate();
        }
    },
    computed: {
        energy_use_max: function () {
            return parseInt(this.energy_use) + 100 - parseInt(this.energy_use_off_peak) - parseInt(this.energy_use_shoulder) - parseInt(this.energy_use);
        },
        energy_use_off_peak_max: function () {
            return parseInt(this.energy_use_off_peak) + 100 - parseInt(this.energy_use_off_peak) - parseInt(this.energy_use_shoulder) - parseInt(this.energy_use);
        },
        energy_use_shoulder_max: function () {
            return parseInt(this.energy_use_shoulder) + 100 - parseInt(this.energy_use_off_peak) - parseInt(this.energy_use_shoulder) - parseInt(this.energy_use);
        },
        system_capacity: function () {
            return (this.system_area * this.efficiency).toFixed(1);

        },
        last_period: function () {
            return this.period_form.length < 2;
        },
        efficiency: function () {
            if (this.panel_efficiency) {
                return 0.21147;
            } else {
                return 0.167;
            }
        },
        peak_no_sun: function () {
            return 100 - this.energy_use_off_peak - this.energy_use_shoulder - this.energy_use;
        },
        map_image: function () {
            return 'https://maps.googleapis.com/maps/api/staticmap?center=' + this.latitude + ',' + this.longitude + '&zoom=20&size=300x300&maptype=satellite&markers=color:red%7Clabel:S%7C' + this.latitude + ',' + this.longitude + '&key=AIzaSyAABWGDhouDXhScoVZGbWjW_WuPwuBQKog'
        },
        all_period_days: function () {
            var days = 0;
            for (var i = 0; i < this.period_form.length; i++) {
                days = days + this.date_delta(this.period_form[i].end_date, this.period_form[i].start_date);
            }
            return days;
        },
        all_period_amount: function () {
            var amount = 0;
            for (var i = 0; i < this.period_form.length; i++) {
                amount = amount + parseInt(this.period_form[i].amount);
            }
            return amount;
        },
        energy_consumption_year: function () {
            return this.all_period_amount / (parseFloat(this.off_peak) * this.energy_use_off_peak / 100 + parseFloat(this.shoulder)
                * this.energy_use_shoulder / 100 + parseFloat(this.peak) * (100 - this.energy_use_off_peak - this.energy_use_shoulder) / 100);
        },
        energy_consumption_9_15: function () {
            return this.energy_consumption_year * this.energy_use / 100;
        },
        energy_consumption_off_peak: function () {
            return this.energy_consumption_year * this.energy_use_off_peak / 100;
        },
        energy_consumption_shoulder: function () {
            return this.energy_consumption_year * this.energy_use_shoulder / 100;
        },
        energy_consumption_peak: function () {
            return this.energy_consumption_year * this.peak_no_sun / 100;
        },
        year_energy: function () {
            var year = 0;
            for (var i = 0; i < this.table_data.length; i++) {
                year = year + parseFloat(this.table_data[i].power);
            }
            return year;
        },
        energy_exported_grid: function () {
            if (this.year_energy - this.energy_consumption_9_15 > 0) {
                return this.year_energy - this.energy_consumption_9_15;
            } else {
                return 0;
            }

        },
        yearly_bill_without_solar: function () {
            return (this.energy_consumption_9_15 + this.energy_consumption_peak) * this.peak + this.energy_consumption_off_peak *
                this.off_peak + this.energy_consumption_shoulder * this.shoulder;
        },
        yearly_bill_with_solar: function () {
            if (this.year_energy - this.energy_consumption_9_15 > 0) {
                return -this.energy_exported_grid * this.feed_in + this.energy_consumption_off_peak * this.off_peak + this.energy_consumption_shoulder *
                    this.shoulder + this.energy_consumption_peak * this.peak;
            } else {
                return (this.energy_consumption_9_15 - this.year_energy) * this.peak + this.energy_consumption_off_peak * this.off_peak + this.energy_consumption_shoulder *
                    this.shoulder + this.energy_consumption_peak * this.peak;
            }
        },
        yearly_bill_without_solar_adjusted: function () {
            return this.yearly_bill_without_solar * (1 + this.return_on_investment * this.electricity_cost_increase / 200);
        },
        yearly_bill_with_solar_adjusted: function () {
            return this.yearly_bill_with_solar * (1 + this.return_on_investment * this.electricity_cost_increase / 200);
        },
        //=B147*(1+B$152*B$27/200)
        system_income: function () {
            return this.yearly_bill_without_solar - this.yearly_bill_with_solar;
        },
        system_income_adjusted: function () {
            return this.yearly_bill_without_solar_adjusted - this.yearly_bill_with_solar_adjusted;
        },
        return_on_investment: function () {
            return (-1 + Math.pow((1 - 4 * (this.increase / 200) * (-(this.system_price) / this.system_income)), 0.5)) / 2 / (this.increase / 200);
            //return ((-2.891 * this.system_capacity) * (-2.891 * this.system_capacity) + 1506.7 * this.system_capacity) / this.system_income;
        },
        //=(-1+(1-4*(B27/200)*(-(B158)/B150))^0,5)/2/(B27/200)
        feed_in_energy_to_grid: function () {
            return this.energy_exported_grid / this.year_energy * 100;
        },
        system_price: function () {
            var result = 0;
            var premium = 1;
            if (this.panel_efficiency && this.system_capacity < 100) {
                premium = -0.00106 * this.system_capacity + 1.1872;
            } else if (this.panel_efficiency && this.system_capacity > 100) {
                premium = 1.0812;
            }
            if (this.system_capacity <= 5) {
                result = 2249.4 * Math.pow(this.system_capacity, 0.4335) * premium;
            }
            if (this.system_capacity > 5 && this.system_capacity <= 7) {
                result = (2444 * this.system_capacity - 7743.2) * premium;
            }
            if (this.system_capacity > 7 && this.system_capacity <= 10) {
                result = (326.6 * this.system_capacity + 7078.5) * premium;
            }
            if (this.system_capacity > 10 && this.system_capacity <= 30) {
                result = (1334.9 * this.system_capacity - 3004.6) * premium;
            }
            if (this.system_capacity > 30 && this.system_capacity < 100) {
                result = ((-2.891 * Math.pow(this.system_capacity, 2) + 1506.7 * this.system_capacity) / 1.15) * premium;
            }
            if (this.system_capacity >= 100) {
                result = 1356 * this.system_capacity * premium;
            }
            return result;
        },
        //=ЕСЛИ(B32<=5; 2249,4*B32^0,4335;0)+ЕСЛИ(И(B32>5;B32<=7); 2444*B32-7743,2;0)+ЕСЛИ(И(B32>7;B32<=10); 326,6*B32+7078,5;0)+ЕСЛИ(И(B32>10;B32<=30); 1334,9*B32-3004,6;0)+ЕСЛИ(И(B32>30;B32<100); (-2,891*B32^2 + 1506,7*B32)/1,15;0)+ЕСЛИ(B32>=100; 1356*B32;0)

        energy_cost: function () {
            return this.system_price / (this.year_energy) / 25;
        },
        energy_cost10: function () {
            return this.system_price / (this.year_energy) / 10;
        },
        energy_exported1: function () {
            if ((this.year_energy - this.energy_consumption_9_15 - this.energy_consumption_peak) > 0) {
                return this.year_energy - this.energy_consumption_9_15 - this.energy_consumption_peak;
            } else {
                return 0;
            }
        },
        energy_exported2: function () {
            if ((this.year_energy - this.energy_consumption_9_15 - this.energy_consumption_peak - this.energy_consumption_shoulder) > 0) {
                return this.year_energy - this.energy_consumption_9_15 - this.energy_consumption_peak - this.energy_consumption_shoulder;
            } else {
                return 1;
            }
        },
        energy_exported3: function () {
            if ((this.year_energy - this.energy_consumption_9_15 - this.energy_consumption_peak -
                    this.energy_consumption_shoulder - this.energy_consumption_off_peak) > 0) {
                return this.year_energy - this.energy_consumption_9_15 - this.energy_consumption_peak -
                    this.energy_consumption_shoulder - this.energy_consumption_off_peak;
            } else {
                return 2;
            }
        },
        battery_size1: function () {
            if (this.energy_consumption_peak / 365 > this.system_capacity) {
                return this.energy_consumption_peak / 365;
            } else {
                return this.system_capacity;
            }

        },
        battery_size2: function () {
            if ((this.energy_consumption_shoulder + this.energy_consumption_peak) / 365 > this.system_capacity * 2) {
                return (this.energy_consumption_peak + this.energy_consumption_shoulder) / 365;
            } else {
                return this.system_capacity * 2;
            }
        },
        battery_size3: function () {
            if ((this.energy_consumption_shoulder + this.energy_consumption_off_peak + this.energy_consumption_peak) / 365 > this.system_capacity * 3) {
                return (this.energy_consumption_shoulder + this.energy_consumption_off_peak + this.energy_consumption_peak) / 365
            } else {
                return this.system_capacity * 3;
            }
        },
        battery_price1: function () {
            if (this.battery_size1 * 1000 < 50001) {
                return this.battery_size1 * 1000 * (704.548201466135 * Math.pow(this.battery_size1 * 1000, -0.795768140790278) + this.cons21);
            } else {
                return this.battery_size1 * 1000 * (67.358 * Math.pow(this.battery_size1 * 1000, -0.583) + this
                    .cons21);
            }
        },
        battery_price2: function () {
            if (this.battery_size2 * 1000 < 50001) {
                return this.battery_size2 * 1000 * (704.548201466135 * Math.pow(this.battery_size2 * 1000, -0.795768140790278) + this.cons21);
            } else {
                return this.battery_size2 * 1000 * (67.358 * Math.pow(this.battery_size2 * 1000, -0.583) + this.cons21);
            }
        },
        battery_price3: function () {
            if (this.battery_size3 * 1000 < 50001) {
                return this.battery_size3 * 1000 * (704.548201466135 * Math.pow(this.battery_size3 * 1000, -0.795768140790278) + this.cons21);
            } else {
                return this.battery_size3 * 1000 * (67.358 * Math.pow(this.battery_size3 * 1000, -0.583) + this.cons21);
            }
        },
        bill_with_pv_battery1: function () {
            var result;
            if (this.year_energy >= (this.energy_consumption_9_15 + this.energy_consumption_peak)) {
                result = (this.energy_consumption_off_peak * this.off_peak + this.energy_consumption_shoulder * this.shoulder - this.energy_exported1 * this.feed_in);
            }
            if (this.year_energy > 0 && this.year_energy < (this.energy_consumption_9_15 + this.energy_consumption_peak)) {
                result = (this.energy_consumption_off_peak * this.off_peak + this.energy_consumption_shoulder * this.shoulder +
                    (this.energy_consumption_9_15 + this.energy_consumption_peak - this.year_energy) * this.peak);
            }
            return result;
        },
        bill_with_pv_battery2: function () {
            var result;
            if (this.year_energy >= (this.energy_consumption_9_15 + this.energy_consumption_peak + this.energy_consumption_shoulder)) {
                result = (this.energy_consumption_off_peak * this.off_peak - this.energy_exported1 * this.feed_in);
            }
            if (this.year_energy >= this.energy_consumption_9_15 + this.energy_consumption_peak && this.year_energy < (this.energy_consumption_9_15 + this.energy_consumption_peak + this.energy_consumption_shoulder)) {
                result = (this.energy_consumption_off_peak * this.off_peak + (this.energy_consumption_9_15 + this.energy_consumption_peak + this.energy_consumption_shoulder - this.year_energy) * this.shoulder);
            }
            if (this.year_energy > 0 && this.year_energy < (this.energy_consumption_9_15 + this.energy_consumption_peak)) {
                result = (this.energy_consumption_off_peak * this.off_peak + this.energy_consumption_shoulder * this.shoulder +
                    (this.energy_consumption_9_15 + this.energy_consumption_peak - this.year_energy) * this.peak);
            }
            return result;
        },
        bill_with_pv_battery3: function () {
            var result;
            if (this.year_energy >= (this.energy_consumption_9_15 + this.energy_consumption_peak + this.energy_consumption_shoulder + this.energy_consumption_off_peak)) {
                result = (-this.energy_exported1 * this.feed_in);
            }
            if ((this.energy_consumption_9_15 + this.energy_consumption_peak + this.energy_consumption_shoulder + this.energy_consumption_off_peak) > this.year_energy && this.year_energy >= (this.energy_consumption_9_15 + this.energy_consumption_peak + this.energy_consumption_shoulder)) {
                result = ((this.energy_consumption_9_15 + this.energy_consumption_peak + this.energy_consumption_shoulder + this.energy_consumption_off_peak - this.year_energy) * this.off_peak);
            }
            if (this.year_energy >= this.energy_consumption_9_15 + this.energy_consumption_peak && this.year_energy < (this.energy_consumption_9_15 + this.energy_consumption_peak + this.energy_consumption_shoulder)) {
                result = (this.energy_consumption_off_peak * this.off_peak + (this.energy_consumption_9_15 + this.energy_consumption_peak + this.energy_consumption_shoulder - this.year_energy) * this.shoulder);
            }
            if (this.year_energy > 0 && this.year_energy < (this.energy_consumption_9_15 + this.energy_consumption_peak)) {
                result = (this.energy_consumption_off_peak * this.off_peak + this.energy_consumption_shoulder * this.shoulder + (this.energy_consumption_9_15 + this.energy_consumption_peak - this.year_energy) * this.peak);
            }
            return result;
        },
         bill_with_pv_battery1_adjusted: function () {
            return this.bill_with_pv_battery1 * (1 + this.return_on_battery1 * this.increase / 200);
        },
        bill_with_pv_battery2_adjusted: function () {
            return this.bill_with_pv_battery2 * (1 + this.return_on_battery2 * this.increase / 200);
        },
        bill_with_pv_battery3_adjusted: function () {
            return this.bill_with_pv_battery3 * (1 + this.return_on_battery3 * this.increase / 200);
        },
        income_with_battery1: function () {
            return this.yearly_bill_without_solar - this.bill_with_pv_battery1;
        },
        income_with_battery1_adjusted: function () {
            return this.income_with_battery1 * (1 + this.return_on_battery1 * this.increase / 200);
        },
        income_with_battery2: function () {
            return this.yearly_bill_without_solar - this.bill_with_pv_battery2;
        },
        income_with_battery2_adjusted: function () {
            return this.income_with_battery2 * (1 + this.return_on_battery2 * this.increase / 200);
        },
        income_with_battery3: function () {
            return this.yearly_bill_without_solar - this.bill_with_pv_battery3;
        },
        income_with_battery3_adjusted: function () {
            return this.income_with_battery3 * (1 + this.return_on_battery3 * this.increase / 200);
        },
        return_on_battery1: function () {
            return (-1 + Math.pow(1 - 4 * (this.increase / 200) * (-(this.system_price + this.battery_price1) /
                this.income_with_battery1), 0.5)) / 2 / (this.increase / 200);
        },
        //=(-1+(1-4*(B31/200)*(-(B190+B155)/B168))^0,5)/2/(B31/200)
        return_on_battery2: function () {
            return (-1 + Math.pow(1 - 4 * (this.increase / 200) * (-(this.system_price + this.battery_price2) /
                this.income_with_battery2), 0.5)) / 2 / (this.increase / 200);
        },
        return_on_battery3: function () {
            return (-1 + Math.pow(1 - 4 * (this.increase / 200) * (-(this.system_price + this.battery_price3) /
                this.income_with_battery3), 0.5)) / 2 / (this.increase / 200);
        },
        feed_in_batary1: function () {
            return this.energy_exported1 / this.year_energy * 100;
        },
        feed_in_batary2: function () {
            return this.energy_exported2 / this.year_energy * 100;
        },
        feed_in_batary3: function () {
            return this.energy_exported3 / this.year_energy * 100;
        },
        increase: function () {
            if (this.electricity_cost_increase === "0") {
                return 0.0000001;
            }
            else {
                return this.electricity_cost_increase;
            }
        }

    },
    methods: {
        recalculate: function () {
            var chart_data = [];
            for (var i = 0; i < this.table_data.length; i++) {
                this.table_data[i].power = (this.table_data[i].solar / 3.6 * (1 - 0.0041 * this.table_data[i].temp) *
                    this.efficiency * this.system_area * (1 - 11.93 / 100) * 31).toFixed(1);
                chart_data.push(this.table_data[i].power);
            }
            this.data1 = chart_data;
        },
        add_period: function () {
            this.period_form.push({
                type: false,
                start_date: "1 January, 2017",
                end_date: "31 December, 2017",
                amount: '1500'
            })
        },
        delete_period: function (index) {
            this.period_form.splice(index, 1);
        },
        set_periods: function () {
            this.period_form = [];
            for (var i1 = 0; i1 < this.periods; i1++) {
                this.period_form.push({type: false, start_date: "17 January, 2015", end_date: "17 January, 2015"})
            }
        },
        getSolar: function () {
            if (this.center) {
                var vm = this;
                fetch('/api?lt=' + this.center.lat() + '&ln=' + this.center.lng()).then(function (response) {
                    response.json().then(function (data) {
                        var chart_data = [];
                        vm.table_data = [];
                        for (var i = 0; i < vm.month.length; i++) {
                            var power = ((data['solar'][i]) / 3.6 * (1 - 0.0041 * data['temperature'][i]) *
                                vm.efficiency * vm.system_area * (1 - 11.93 / 100) * 31).toFixed(1);
                            vm.table_data.push({
                                month: vm.labels[i],
                                solar: data['solar'][i],
                                temp: data['temperature'][i],
                                power: power
                            });
                            chart_data.push(power);
                        }
                        vm.data1 = chart_data;
                    });
                });
            }
        },
        date_delta: function (end, start) {
            var start_date = new Date(end);
            var end_date = new Date(start);
            return (start_date.getTime() - end_date.getTime()) / 1000 / 60 / 60 / 24 + 1;
        }

    }
});

//--------------------------------------------------------------
$(document).ready(function () {
    $('ul.tabs').tabs('select_tab', '#test-swipe-1');
});
$(document).ready(function () {

});
$(document).ready(function () {
    var collaps = $('.collapsible');
    collaps.collapsible({
        onOpen: function (el) {
            google.maps.event.trigger(map, 'resize');
        },
        onOpenStart: function (el) {
        }
    });
    $("#next1").click(function () {
        //collaps.collapsible('open', 1);
        $("#amount1").focus();
    });
    $("#next2").click(function () {
        collaps.collapsible('open', 2);
        $('ul.tabs').tabs('select_tab', '#test-swipe-1');
    });
    $('.modal').modal();
    $(".button-collapse").sideNav();

});



