
var mapCanvas, boxes = new google.maps.MVCArray();
function initialize() {
  var mapDiv = document.getElementById("map_canvas");
  mapCanvas = new google.maps.Map(mapDiv, {
    center : new google.maps.LatLng(41.796868, 140.756794),
    mapTypeId : google.maps.MapTypeId.SATELLITE,
    zoom : 16
  });


  //Encoded path
  var encodedPath = "uyb~FupbzYqAoK|H{@~FaFjCxInFdGkBlEn@|FeEPmA|EaGsDiIUr@_L";
  var points = google.maps.geometry.encoding.decodePath(encodedPath);

  //Draw a polygon
  var polygonOpts = {
    paths: points,
    strokeWeight : 6,
    strokeColor : "#FF0000",
    strokeOpacity : 1,
    fillColor: "blue",
    fillOpacity: 0.3,
    map : mapCanvas,
    editable : true
  };
  var poly = new google.maps.Polygon(polygonOpts);

  onPolygonComplete(poly);

  var proc = function() {
    onPolygonComplete(poly);
  };
  google.maps.event.addListener(poly.getPath(), 'insert_at', proc);
  google.maps.event.addListener(poly.getPath(), 'remove_at', proc);
  google.maps.event.addListener(poly.getPath(), 'set_at', proc);
}

function onPolygonComplete(polygon) {
  var bounds, paths, sw, ne, ystep, xstep,
      boxH, boxW, posArry, flag, pos,
      x, y, i, box, maxBoxCnt;

  //Delete old boxes.
  boxes.forEach(function(box, i) {
    box.setMap(null);
    delete box;
  });

  //Calculate the bounds that contains entire polygon.
  bounds = new google.maps.LatLngBounds();
  paths = polygon.getPath();
  paths.forEach(function(latlng, i){
    bounds.extend(latlng);
  });


  new google.maps.Rectangle({
    bounds : bounds,
    map : mapCanvas,
          strokeColor: '#ffff00',
          strokeOpacity: 0.5,
          strokeWeight: 5
  });

  //Calculate the small box size.
  maxBoxCnt = 8;
  sw = bounds.getSouthWest();
  ne = bounds.getNorthEast();
  ystep = Math.abs(sw.lat() - ne.lat()) / maxBoxCnt;
  boxH = Math.abs(sw.lat() - ne.lat()) / (maxBoxCnt + 1);
  xstep = Math.abs(sw.lng() - ne.lng()) / maxBoxCnt;
  boxW = Math.abs(sw.lng() - ne.lng()) / (maxBoxCnt + 1);

  for (y = 0; y < maxBoxCnt; y++) {
    for (x = 0; x < maxBoxCnt; x++) {
      //Detect that polygon is able to contain a small box.
      bounds = new google.maps.LatLngBounds();
      posArry = [];
      posArry.push(new google.maps.LatLng(sw.lat() + ystep * y, sw.lng() + xstep * x));
      posArry.push(new google.maps.LatLng(sw.lat() + ystep * y, sw.lng() + xstep * x + boxW));
      posArry.push(new google.maps.LatLng(sw.lat() + ystep * y + boxH, sw.lng() + xstep * x));
      posArry.push(new google.maps.LatLng(sw.lat() + ystep * y + boxH, sw.lng() + xstep * x + boxW));
      flag = true;
      for (i = 0; i < posArry.length; i++) {
        pos = posArry[i];
        if (flag) {
          flag = google.maps.geometry.poly.containsLocation(pos, polygon);
          bounds.extend(pos);
        }
      }

      //Draw a small box.
      if (flag) {
        box = new google.maps.Rectangle({
          bounds : bounds,
          map : mapCanvas,
          strokeColor: '#00ffff',
          strokeOpacity: 0.5,
          strokeWeight: 1,
          fillColor: '#00ffff',
          fillOpacity : 0.5,
          clickable: false
        });
        boxes.push(box);
      }
    }
  }
}


google.maps.event.addDomListener(window, "load", initialize);
