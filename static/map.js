var map;

// Create a meausure object to store our markers, MVCArrays, lines and polygons
var measure = {
    mvcLine: new google.maps.MVCArray(),
    mvcPolygon: new google.maps.MVCArray(),
    mvcMarkers: new google.maps.MVCArray(),
    line: null,
    polygon: null
};
var mapCanvas, boxes = new google.maps.MVCArray();

var listener1 = null, listener2 = null, listener3 = null;

function ClearControl(controlDiv, map) {

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#ffcdd2';
    controlUI.style.border = '2px solid #ffcdd2';
    controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginBottom = '22px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to clear map';
    controlUI.classList.add('btn');
    controlUI.classList.add('red');
    controlUI.classList.add('waves-effect');
    controlUI.classList.add('waves-light');
    // controlUI.classList.add('col');
    //controlUI.classList.add('s1');
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(255,255,255)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = 'Clear Map';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', function () {
        measureReset();
        google.maps.event.trigger(map, 'resize');
    });

}


function RefreshControl(controlDiv, map) {

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#ffcdd2';
    controlUI.style.border = '2px solid #ffcdd2';
    controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginBottom = '22px';
    controlUI.style.marginLeft = '12px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Next';
    controlUI.classList.add('btn');
    controlUI.classList.add('blue');
    controlUI.classList.add('waves-effect');
    controlUI.classList.add('waves-light');
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(255,255,255)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = 'Next';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', function () {
        //google.maps.event.trigger(map, 'resize');
        $("#amount1").focus();
    });

}

jQuery(document).ready(function () {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -37.732791057316405, lng: 145.00231850892305},
        zoom: 21,
        mapTypeId: 'satellite',
        draggableCursor: "crosshair",
        disableDefaultUI: true,
        zoomControl: true,
    });
    var input = (document.getElementById('search'));
    var search_box = (document.getElementById('search_box'));


    google.maps.event.addListener(map, "click", function (evt) {
        // When the map is clicked, pass the LatLng obect to the measureAdd function
        measureAdd(evt.latLng);
    });

    var clearControlDiv = document.createElement('div');
    var clearControl = new ClearControl(clearControlDiv, map);
    var refreshControlDiv = document.createElement('div');
    var refreshControl = new RefreshControl(refreshControlDiv, map);

    clearControlDiv.index = 1;
    refreshControlDiv.index = 2;
    map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(clearControlDiv);
    map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(refreshControlDiv);
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(search_box);
     var infowindow = new google.maps.InfoWindow({map: map});
     var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });
      infowindow.close();
 var autocomplete = new google.maps.places.Autocomplete(input);
 autocomplete.bindTo('bounds', map);

autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
          }));
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
          infowindow.open(map, marker);
        });



    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            //infoWindow.setPosition(pos);
            //infoWindow.setContent('Location found.');


            //map.setCenter(pos);
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
});

function onPolygonComplete(polygon) {
    var bounds, paths, sw, ne, ystep, xstep,
        boxH, boxW, posArry, flag, pos,
        x, y, i, box, maxBoxCnt;

    //Delete old boxes.
    boxes.forEach(function (box, i) {
        box.setMap(null);
        delete box;
    });

    //Calculate the bounds that contains entire polygon.
    bounds = new google.maps.LatLngBounds();
    paths = polygon.getPath();
    paths.forEach(function (latlng, i) {
        bounds.extend(latlng);
    });


    // new google.maps.Rectangle({
    //   bounds : bounds,
    //   map : map,
    //         strokeColor: '#ffff00',
    //         strokeOpacity: 0.5,
    //         strokeWeight: 5
    // });


    //Calculate the small box size.
    maxBoxCnt = 8;
    sw = bounds.getSouthWest();
    ne = bounds.getNorthEast();
    ystep = Math.abs(sw.lat() - ne.lat()) / maxBoxCnt;
    boxH = Math.abs(sw.lat() - ne.lat()) / (maxBoxCnt + 1);
    xstep = Math.abs(sw.lng() - ne.lng()) / maxBoxCnt;
    boxW = Math.abs(sw.lng() - ne.lng()) / (maxBoxCnt + 1);

    for (y = 0; y < maxBoxCnt; y++) {
        for (x = 0; x < maxBoxCnt; x++) {
            //Detect that polygon is able to contain a small box.
            bounds = new google.maps.LatLngBounds();
            posArry = [];
            posArry.push(new google.maps.LatLng(sw.lat() + ystep * y, sw.lng() + xstep * x));
            posArry.push(new google.maps.LatLng(sw.lat() + ystep * y, sw.lng() + xstep * x + boxW));
            posArry.push(new google.maps.LatLng(sw.lat() + ystep * y + boxH, sw.lng() + xstep * x));
            posArry.push(new google.maps.LatLng(sw.lat() + ystep * y + boxH, sw.lng() + xstep * x + boxW));
            flag = true;
            for (i = 0; i < posArry.length; i++) {
                pos = posArry[i];
                if (flag) {
                    flag = google.maps.geometry.poly.containsLocation(pos, polygon);
                    bounds.extend(pos);
                }
            }

            //Draw a small box.
            if (flag) {
                box = new google.maps.Rectangle({
                    bounds: bounds,
                    map: map,
                    strokeColor: '#00ffff',
                    strokeOpacity: 0.5,
                    strokeWeight: 1,
                    fillColor: '#00ffff',
                    fillOpacity: 0.5,
                    clickable: false
                });
                boxes.push(box);
            }
        }
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
}

function measureAdd(latLng) {
    // if (measure.mvcPolygon.getLength() < 4) { ---------------------------- use this if you want to limit the polygon to four corners
    // Add a draggable marker to the map where the user clicked
    var marker = new google.maps.Marker({
        map: map,
        position: latLng,
        draggable: true,
        raiseOnDrag: false,
        title: "Drag this to change shape",
        icon: new google.maps.MarkerImage("/static/vertex2.png", new google.maps.Size(20, 20), new google.maps.Point(0, 0), new google.maps.Point(9, 9))
    });
    // }

    // Add this LatLng to our line and polygon MVCArrays
    // Objects added to these MVCArrays automatically update the line and polygon shapes on the map
    measure.mvcLine.push(latLng);
    // if (measure.mvcPolygon.getLength() < 4) { ---------------------------- use this if you want to limit the polygon to four corners
    measure.mvcPolygon.push(latLng);
    // }

    // Push this marker to an MVCArray
    // This way later we can loop through the array and remove them when measuring is done
    measure.mvcMarkers.push(marker);

    // Get the index position of the LatLng we just pushed into the MVCArray
    // We'll need this later to update the MVCArray if the user moves the measure vertexes
    var latLngIndex = measure.mvcLine.getLength() - 1;

    // When the user mouses over the measure vertex markers, change shape and color to make it obvious they can be moved
    google.maps.event.addListener(marker, "mouseover", function () {
        marker.setIcon(new google.maps.MarkerImage("/static/vertex_hover4.png", new google.maps.Size(20, 20), new google.maps.Point(0, 0), new google.maps.Point(9, 9)));
    });

    // Change back to the default marker when the user mouses out
    google.maps.event.addListener(marker, "mouseout", function () {
        marker.setIcon(new google.maps.MarkerImage("/static/vertex2.png", new google.maps.Size(20, 20), new google.maps.Point(0, 0), new google.maps.Point(9, 9)));
    });

    // When the measure vertex markers are dragged, update the geometry of the line and polygon by resetting the
    //     LatLng at this position
    google.maps.event.addListener(marker, "drag", function (evt) {
        measure.mvcLine.setAt(latLngIndex, evt.latLng);
        measure.mvcPolygon.setAt(latLngIndex, evt.latLng);
    });

    // When dragging has ended and there is more than one vertex, measure length, area.
    google.maps.event.addListener(marker, "dragend", function () {
        if (measure.mvcLine.getLength() > 1) {
            measureCalc();
        }
    });

    // // If there is more than one vertex on the line
    if (measure.mvcLine.getLength() > 1) {

        //     // If the line hasn't been created yet
        if (!measure.line) {

            //         // Create the line (google.maps.Polyline)
            measure.line = new google.maps.Polyline({
                map: map,
                clickable: false,
                strokeColor: "#FF0000",
                strokeOpacity: 1,
                strokeWeight: 2,
                path: measure.mvcLine
            });

        }
    }

    // If there is more than two vertexes for a polygon
    if (measure.mvcPolygon.getLength() > 2) {

        // If the polygon hasn't been created yet
        if (!measure.polygon) {

            // Create the polygon (google.maps.Polygon)
            measure.polygon = new google.maps.Polygon({
                clickable: false,
                map: map,
                fillOpacity: 0.2,
                strokeColor: "#FF0000",
                strokeOpacity: 0.6,
                strokeWeight: 2,
                paths: measure.mvcPolygon,
                draggable: true,
                geodesic: false
            });
            onPolygonComplete(measure.polygon);
            var proc = function () {
                onPolygonComplete(measure.polygon);
            };
            listener1 = google.maps.event.addListener(measure.polygon.getPath(), 'insert_at', proc);
            listener2 = google.maps.event.addListener(measure.polygon.getPath(), 'remove_at', proc);
            listener3 = google.maps.event.addListener(measure.polygon.getPath(), 'set_at', proc);


        }

    }

    // }

    // If there's more than one vertex, measure length, area.
    if (measure.mvcPolygon.getLength() > 1) {
        measureCalc();
    }

}

function measureCalc() {
    // Use the Google Maps geometry library to measure the length of the line
    // var length = google.maps.geometry.spherical.computeLength(measure.line.getPath());
    // jQuery("#span-length").text(length.toFixed(1))

    // If we have a polygon (>2 vertexes in the mvcPolygon MVCArray)


    if (measure.mvcPolygon.getLength() > 2) {
        app.show_data = true;
        var bounds = new google.maps.LatLngBounds();
        var vertices = measure.polygon.getPath();
        for (var i = 0; i < vertices.getLength(); i++) {
            bounds.extend(vertices.getAt(i));
        }
        var center = bounds.getCenter();
        app.center = center;
        app.longitude = center.lng();
        app.latitude = center.lat();
        app.getSolar();
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({'location': center}, function (results, status) {
            if (status === 'OK') {
                if (results[1]) {
                    app.address = results[1].formatted_address;
                }
            }
        });
        // Use the Google Maps geometry library to measure the area of the polygon
        var moduleTypeFactor = .167;

        var moduleType = $("#module_type").val();
        if ((moduleType === undefined) || (moduleType == "Standard")) {
            moduleTypeFactor = .167;
        } else if (moduleType == "Premium") {
            moduleTypeFactor = .211;
        } else if (moduleType == "Thin-Film") {
            moduleTypeFactor = .10;
        }

        var area = google.maps.geometry.spherical.computeArea(measure.polygon.getPath());
        var areaval = area.toFixed(1) * 1000 * moduleTypeFactor * 0.001;
        if (areaval > 500000) {
            $("#draw-area").text("ERROR: Area too large, set to max: 500,000 kWdc.");
            $("#areaval").val(500000);
        } else if (areaval < 0.05) {
            $("#draw-area").text("ERROR: Area too small, set to min: 0.05 kWdc.");
            $("#areaval").val(0.05);
        } else {
            //$("#draw-area").html(areaval.toFixed(1) + " kWdc  (" + area.toFixed(0) + " m<sup>2</sup>)");
            $("#draw-area").html(area.toFixed(0) + " m<sup>2</sup>");
            $("#areaval").val(areaval.toFixed(1));
            app.system_area = area.toFixed(0);
        }
    }

}

function measureReset() {

    // If we have a polygon or a line, remove them from the map and set null
    if (measure.polygon) {
        listener1.remove();
        listener2.remove();
        listener3.remove();
        boxes.forEach(function (box, i) {
            box.setMap(null);
            delete box;
        });
        measure.polygon.setMap(null);
        measure.polygon = null;
    }
    if (measure.line) {
        measure.line.setMap(null);
        measure.line = null
    }

    // Empty the mvcLine and mvcPolygon MVCArrays
    measure.mvcLine.clear();
    measure.mvcPolygon.clear();

    // Loop through the markers MVCArray and remove each from the map, then empty it
    measure.mvcMarkers.forEach(function (elem, index) {
        elem.setMap(null);
    });
    measure.mvcMarkers.clear();

    //map.setZoom(21);

    jQuery("#span-length,#span-area,#draw-area").text(0);

}